//
//  TableViewCell.swift
//  Cocktails
//
//  Created by tomaszpaluch on 03/10/2019.
//  Copyright © 2019 tomaszpaluch. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var checkLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
}

